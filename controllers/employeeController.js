
const express = require('express');
var router = express.Router({ mergeParams: true });
const mongoose = require('mongoose');
const Employee = mongoose.model('Employee');

router.get('/',(req,res) => {
   
    res.render('employee/addOrEdit',{
        viewTitle : 'Insert Employee',
        csrfToken : req.csrfToken()
    });
});
router.post('/',(req,res) => {
    if(req.body._id == ''){
        insertRecord(req,res);
    } else {
        updateRecord(req,res);
    }
})

function insertRecord(req,res){
    var employee = new Employee();
    employee.fullName = req.body.fullName;
    employee.email = req.body.email;
    employee.mobile = req.body.mobile;
    employee.save((err,doc) => {
        if(!err){
            //res.redirect('employee/list');
            res.send(true);
        } else {
            res.send(err);
            // console.log("else");
            // console.log(err.name);
            // if(err.name == 'ValidationError'){
            //     handleValidationError(err,req.body);
            //     console.log(req.body);
            //     res.render('employee/addOrEdit',{
            //         viewTitle : 'Insert Employee',
            //         employee: req.body
            //     });
            // } else {
            //     console.log('Error generated during adding records : ' + err);
            // }
        }
    });
}
function updateRecord(req,res){
    Employee.findOneAndUpdate({_id: req.body._id},req.body,{new: true},(err,doc) => {
        if(!err){
            res.redirect('/employee/list');
        } else {
            if(err.name = 'ValidationError'){
                handleValidationError(err,req.body);
                res.render('employee/addOrEdit',{
                    viewTitle : 'Update Employee', 
                    employee: req.body
                });
            } else {
                console.log("Error while updating :"+ err);
            }
        }
    });
}
function handleValidationError(err,body){
    for(field in err.erros){
        switch(err.errors[field].path){
            case 'fullName':
                body['fullNameError'] = err.errors[field].message;
                break;
            case 'email':
                body['emailError'] = err.errors[field].message;
                break;
            default:
                break;
        }
    }
}
router.get('/list',(req,res) => {
    Employee.find().lean().exec((err,docs) => {
        if(!err){
            // res.render('employee/list',{
            //     list: docs
            // });
            res.send(docs);
        } else {
            console.log("Error generated while listing :"+ err);
        }
    })
});

router.get('/view/:id',(req,res) => {
    Employee.findById(req.params.id,(err,doc) => {
        
        if(!err){
            res.send(doc);
        } else {
            res.send("error");
        }
    });
});
router.get('/:id',(req,res) => {
    Employee.findById(req.params.id,(err,doc) => {
        console.log(doc);
        console.log("inside edit");
        if(!err){
            const context = {
                
                    fullName: doc.fullName,
                    email: doc.email,
                    id: doc.id,
                    mobile: doc.mobile
                  
              }
              console.log(context);
            res.render('employee/addOrEdit',{
                viewTitle : 'Update Employee', 
                employee: context
            });
        } else {
            console.log("error");
        }
    });
   
});

router.get('/delete/:id',(req,res) => {
    Employee.findByIdAndRemove(req.params.id,(err,doc) => {
        
        if(!err){
            res.redirect('/employee/list');
        } else {
            console.log("Error while deleting :"+ err);
        }
    });
});
module.exports = router;